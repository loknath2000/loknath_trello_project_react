import Trello from "./components/Trello"
import React from 'react'
import './App.css';

function App() {
  return (
    <div className="App">
      <h1 className="trelloTitle">Trello</h1>
      <Trello/>
    </div>
  );
}

export default App;
