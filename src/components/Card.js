import React, { useState } from 'react';
import "./Trello.css"
import Popup from './Popup';


const key="cbdb1f25866b842989356e653abdc925";
const token="fc4aff7f90dc80d9c9d2d69d999967daaaaf17aa737f5e67cdbb81b27af72abe"
let cardID
let listID


let cardId;
let listId;
let nameOfList;
let drag_item;
const Card = (props) => {

    const [showPop , setShowPop] = useState(false);

    cardId =props.cardId
    console.log(cardId)
    let nameOfCard=props.name
    console.log(nameOfCard)
    listId=props.listId
    nameOfList=props.listName

    console.log(nameOfList)
    console.log(listId)

// funcitons for drag and drop

const drag_start = (event) => {
   // event.preventDefault()
   drag_item = event.target
   cardID = event.target.getAttribute("id")
   console.log(cardID)
 }
 const drag_enter = () => {
   console.log('drag enter')
 }
 const drag_over = (event) => {
   event.preventDefault()
   console.log('drag over')
 }
 const drag_drop = (event) => {
   event.target.parentNode.append(drag_item)
    listID= event.target.parentNode.parentNode.parentNode.firstElementChild.firstElementChild.getAttribute("id")
   
    async function updatingDrag() {
        let response = await fetch(`https://api.trello.com/1/cards/${cardID}?pos=bottom&idList=${listID}&key=${key}&token=${token}`, {
                   method: "PUT"
               })
               return response.json();
           }
           updatingDrag()
               .then(text => console.log(text))
   console.log(event.target.parentNode.parentNode.parentNode.firstElementChild.firstElementChild.getAttribute("id"))
 }
        
    return (
        <div className='card-container' >
               <p className="card" id={cardId[nameOfCard]} draggable="true"  onClick={()=>setShowPop(true)} onDragStart={drag_start} onDrop={drag_drop} onDragOver={drag_over} onDragEnter={drag_enter} >{props.name} </p>
    
               <div className='pops'>
                <Popup showPop={showPop} closePop={()=>setShowPop(false)}  name={props.name} cardId={cardId}></Popup>
               </div>
        </div>
    );
};

export default Card;