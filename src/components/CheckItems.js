import React,{useEffect,useState} from 'react';


const key="cbdb1f25866b842989356e653abdc925";
const token="fc4aff7f90dc80d9c9d2d69d999967daaaaf17aa737f5e67cdbb81b27af72abe"

let cardId
let checkListId
let checkItemsId
let cardName
const CheckItems = (props) => {
    let checkListTitle = props.checkListTitle
    let checkItemName= props.item
    cardName=props.cardName
    cardId=props.cardId
    checkListId = props.checkListId
    checkItemsId =props.checkItemsId

    const [checked,setChecked] = useState(false)
    const [checkComplete,setCheckComplete]= useState(false)
    const [clicked,setClicked] = useState("Incomplete")

    console.log(props.item)

    //  function for the checkbox functioning and fetching it the same
    const clickedCheckBox=()=>{
        !checked?setChecked(true):setChecked(false)
        
        checked?setCheckComplete(true):setCheckComplete(false)
        !checkComplete?setClicked('Incomplete'):setClicked('complete')
        fetch(`https://api.trello.com/1/cards/${cardId[cardName]}/checklist/${checkListId[checkListTitle]}/checkItem/${checkItemsId[checkItemName]}?state=${checkComplete}&key=${key}&token=${token}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json'
                }
                })
                .then(response => {
                    console.log(
                    `Response: ${response.status} ${response.statusText}`
                    );
                    return response.text();
                })
                .then(text => console.log(text))
                .catch(err => console.error(err));
    }

    // getting the items after checked

    useEffect(()=>{
        async function gettingCheckItems(){
            let response = await fetch(`https://api.trello.com/1/checklists/${checkListId[checkListTitle]}/checkItems?key=${key}&token=${token}`, {
                    method: 'GET'
            })
            return response.json();
        }
        gettingCheckItems()
            .then(items=>{
                items.forEach(element => {
                    setClicked(element.state)
                    if (element.state==="complete"){
                        setChecked(true)
                    }
                });
              
            })
    })
    return (
        <div className='checkItems'>
        <input type='checkbox' onClick={clickedCheckBox}></input>
        <p style={{textDecoration:checked?"line-through":"none"}}>{props.item}</p>
                                                
        </div>
    );
};

export default CheckItems;