import React, { useEffect, useState}from 'react';
import Card from './Card';
import "./Trello.css"

const key="cbdb1f25866b842989356e653abdc925";
const token="fc4aff7f90dc80d9c9d2d69d999967daaaaf17aa737f5e67cdbb81b27af72abe"


const cardId = {};
let listId;
const List = (props) => {
    const [card,setCard] = useState("");
    const [cardName, setCardName] = useState([])   

     
    const addCard = (e)=>{
        setCard(e.target.value)
    }
    let nameOfList = props.datas
    listId = props.listId

    // function to add the cards in to list and fetching to trello
    const clickedAddButton = (e) =>{
        let newCard = [...cardName,card];
        setCardName(newCard);
            fetch(`https://api.trello.com/1/cards?name=${card}&idList=${listId[nameOfList]}&key=${key}&token=${token}`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json'
                }
                })
                .then(response => {
                    console.log(
                    `Response: ${response.status} ${response.statusText}`
                    );
                    return response.text();
                })
                .then(text => {
                    let obj= JSON.parse(text);
                    cardId[card] = obj.id;
                    // console.log(cardId)
                    // console.log(obj)
                })
                // .catch(err => console.error(err));
        setCard("")
    }

     
// function to remove list after clicking the x mark
    
    const removedList = (e) =>{
        e.target.parentElement.parentElement.remove();

        fetch(`https://api.trello.com/1/lists/${listId[nameOfList]}/closed?value=true&key=${key}&token=${token}`, {
                method: 'PUT'
            })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
        // .then(text => console.log(text))
        // .catch(err => console.error(err));
    }

    //  getting the cards from a list using fetching


    useEffect (()=>{
        async function getCards() {
            let response = await fetch(`https://api.trello.com/1/lists/${listId[nameOfList]}/cards?key=${key}&token=${token}`, {
                method: 'GET'
            })
            return response.json();
        }
        
        getCards()
        .then(text=>{
            let cards=[]
           
            text.forEach(card=>{
                cards.push(card.name)
               
                cardId[card.name]= card.id
            })
            
            setCardName(cards)
        })
    },[])
    console.log(cardId)
    
            return (
                <div className='main'>
        <div className='mainList'>
            <div className='listTrello'>
            <div className='listHeader'>
            <h4 className='listHeading' id={listId[nameOfList]}>{props.datas}</h4>
            <input className='removingList' type="button" value="X" onClick={removedList} />
            </div>
            <div className='cards'>
            {
                cardName.map((cards,index)=>{
                    return <Card  key={index} name={cards} cardId={cardId} listId={listId} listName={props.datas}/>
                })
            }
            </div>
            <div className='cardAdd'>
                <input className='cardInput' type="text"  placeholder="add a card" onChange={addCard} value={card}/>
                <input className='cardAddButton' type="button" value="Add" onClick={clickedAddButton} />
            </div>
        </div>
    </div>
    </div>
    );
            }

export default List;