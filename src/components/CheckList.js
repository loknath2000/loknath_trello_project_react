import React,{useEffect, useState}  from 'react';
import CheckItems from "./CheckItems"


const key="cbdb1f25866b842989356e653abdc925";
const token="fc4aff7f90dc80d9c9d2d69d999967daaaaf17aa737f5e67cdbb81b27af72abe"
let checkListId
let cardId
let checkItemsId={}
// let cardName
const CheckList = (props) => {

    checkListId = props.checkListId
    cardId=props.cardId
    // cardName=props.cardName
    let checkListName=props.title
    const [checkedValue, setCheckedValue] = useState("");
    const [checkedArray, setCheckedArray] = useState([]);

        const checkedInput = (e)=>{
            setCheckedValue(e.target.value);
        }

        console.log(props.checkListId)

        //  function to adding checklist titiel in to array and fetching each time

        const clickAddingCheckList=()=>{
           
            
            const newChecked = [...checkedArray,checkedValue]
            setCheckedArray(newChecked);
          
            
            
            fetch(`https://api.trello.com/1/checklists/${checkListId[checkListName]}/checkItems?name=${checkedValue}&key=${key}&token=${token}`, {
                method: 'POST'
            })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                    );
                    return response.text();
                })
                .then(text => {

                    checkItemsId[checkedValue]=JSON.parse(text).id
                    })
                // .catch(err => console.error(err));
                setCheckedValue("")
            }

            // getting the checkitems from checklists 

    useEffect(()=>{
        async function gettingCheckItems(){
            let response = await fetch(`https://api.trello.com/1/checklists/${checkListId[checkListName]}/checkItems?key=${key}&token=${token}`, {
                    method: 'GET'
            })
            return response.json();
        }
        gettingCheckItems()
            .then(text=>{
                let items = [];
                text.forEach((item)=>{
                    items.push(item.name)
                    checkItemsId[item.name]=item.id
                })
                setCheckedArray(items)
            })
    },[])



    return (
        <div>
             <div className='checkListTitle'>
             <div className='titles'>

             <i class="fa-solid fa-square-check" ></i>
                <h4 >{props.title}</h4>
             </div>
                <input type="button" className="deleteCheckList" onClick={props.deleteCheckList}  value="Delete" />
             </div>
             <div className='itemInput'>
                 <input type='text' name='checklist' placeholder='Add a checklist' value={checkedValue} onChange={checkedInput}/>
                 <input type='button' className='checkListAdd' onClick={clickAddingCheckList} value="Add and item"/>
                 {
                     checkedArray.map(items=>{
                        return <CheckItems item = {items} cardId={cardId} checkItemsId={checkItemsId} checkListId={checkListId} checkListTitle={props.title} cardName={props.cardName}/>
                     })
                 }
            </div>
        </div>
    );
};

export default CheckList;