import React, { useState} from "react";
import "./Trello.css"
import List from "./List"
import RenderList from "./RenderList";

const key="cbdb1f25866b842989356e653abdc925";
const token="fc4aff7f90dc80d9c9d2d69d999967daaaaf17aa737f5e67cdbb81b27af72abe"
const boardId = "624a743242f14533470abeb5";
let listId={}

// let listArray = [];
const Trello = ()=>{
    const [list,setList] = useState("");
    const [data,setData] = useState([])
    


    const changeHandler = (e)=>{
    setList(e.target.value)
  }
//   function to submit the list and fetching in to trello
  const submitHandler = (e)=>{
      e.preventDefault();
        const newList = [...data, list];
        setData(newList);

        fetch(`https://api.trello.com/1/lists?name=${list}&idBoard=${boardId}&key=${key}&token=${token}`, {
            method: 'POST'
        })
            .then(response => {
                console.log(
                `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text =>{ 
                let obj = JSON.parse(text)
                listId[list]=obj.id
                console.log(listId)
            })
            .catch(err => console.error(err));
            setList("");
        }

    return(
        <div className="container">
            <form onSubmit={submitHandler} className='listForm'>
                <input type="text" name="text" value={list} onChange={changeHandler} placeholder="+ Add a list"/>
                <input className="formButton" type="submit" name="ADD" value="ADD"/>
            </form>
            <div className="list">

            {data.map((datas,index)=>{
                return <List key={index} datas = {datas} listId={listId}/>
                
            }
            )}
            </div>
            <div className="list">

            <RenderList/> 
            </div>
            
        </div>
        
    )
}

export default Trello;