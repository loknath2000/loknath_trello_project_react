import React, {useEffect, useState } from 'react';
import "./Trello.css"

import CheckList from './CheckList';



const key="cbdb1f25866b842989356e653abdc925";
const token="fc4aff7f90dc80d9c9d2d69d999967daaaaf17aa737f5e67cdbb81b27af72abe"


let checkListId = {};
let cardId;
// let cardName;
let commentIds={};
let desc;

const Popup = (props) => {

     cardId = props.cardId
    console.log(cardId)

     let cardName=props.name;
    console.log(cardName)
    

    const [descriptionValue, setDescriptionValue] = useState("");

    const [edit , setEdit] = useState("")

    const [addDescription, setAddDescription] = useState(false)
    const [descriptionInput, setDescriptionInput] = useState(false);

    // const desc = descriptionValue;

    const [titleValue,setTitleValue] = useState("")
    const [titleArray, setTitleArray] = useState([])
    const [addTitle, setAddTitle] = useState(false);



    const [commentValue, setCommentValue] = useState("");
    const [commentArray, setCommentArray] = useState([])


    // const [checkList, setCheckList] = useState(false);
    // let title = titleValue;

    const commentInput = (e) =>{
        setCommentValue(e.target.value)
    }
// function to save the comments and fetching that comments
    const clickedCommentSaved = () =>{
        // setComment(true);
        let newComment = [...commentArray,commentValue]
        setCommentArray(newComment)

        
        fetch(`https://api.trello.com/1/cards/${cardId[cardName]}/actions/comments?text=${commentValue}&key=${key}&token=${token}`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json'
                }
            })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text=>{
                let obj=JSON.parse(text)
                commentIds[commentValue]=obj.id
            })

        setCommentValue("")
    }
console.log("helloo",commentIds)

// function to updating the description 
    const clickedSave = (e) =>{
        setDescriptionInput(true)
        desc = e.target.previousSibling.value
        // cardName = e.target.parentElement.previousSibling.firstElementChild.textContent
        fetch(`https://api.trello.com/1/cards/${cardId[cardName]}?key=${key}&token=${token}&desc=${desc}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json'
                }
            })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
        setEdit("")
    }

    // getting description data from card

    useEffect(()=>{
          async function descriptData() {
        
        let response = await fetch(`https://api.trello.com/1/cards/${cardId[cardName]}?key=${key}&token=${token}`, {
            method: "GET"
        })
        return response.json();
    }
    
    descriptData()
    .then(text => {
        // desc = text.desc
        // setDescriptionInput(true)
        setDescriptionValue(text.desc)
        console.log(text)
    })
     },[])

    //  end for getting description data


    // getting the comments data


    useEffect(()=>{
        async function commentData() {
            
            let response = await fetch(`https://api.trello.com/1/cards/${cardId[cardName]}/actions?key=${key}&token=${token}`, {
                method: "GET"
            })
            return response.json();
        }
        
        commentData()
        .then(text=>{
            let newComment=[]
            text.forEach(element => {
                newComment.push(element.data.text)
                commentIds[element.data.text]=element.id
                // console.log(element.id)  
            });
            setCommentArray(newComment)
        })
        
    },[])




    // ending the getting comments data



// ADDING CHECKLIST 
    const clickedAddTitle = (e) =>{
        e.preventDefault();
       
        let newTitle = [...titleArray,titleValue];
        setTitleArray(newTitle);
        console.log(titleArray)
        if (titleValue === "")return
        
        fetch(`https://api.trello.com/1/checklists?name=${titleValue}&idCard=${cardId[cardName]}&key=${key}&token=${token}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json'
            }
        })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
        .then(text=>{
            let obj = JSON.parse(text)
            checkListId[titleValue] = obj.id;
        })
        setTitleValue("")
        
        
    }
    console.log("-------------------",checkListId)

    // getting checklists in card
    useEffect(()=>{
        async function gettingChecklists() {
        let response = await fetch(`https://api.trello.com/1/cards/${cardId[cardName]}/checklists?key=${key}&token=${token}`, {
            method: 'GET'
        })
        return response.json();
    }
    gettingChecklists()
        .then(text=>{
            let newCheckList = [];
            text.forEach(checkList=>{
               
                newCheckList.push(checkList.name)
                checkListId[checkList.name] = checkList.id

            })
        
     
        setTitleArray(newCheckList)
            
        })
        },[])
        
        // ending of getting checklist of card
        
        
        // adding checklist content
        
        
        
        
            
            // getting checkItems in checklist 
            
            
    




    // ending of checkitems getting in checklists

    const checkHandler = (e) =>{
        setTitleValue(e.target.value)
    }
    

    const clickedCheckBtn = (e) =>{
        setAddTitle(true)
    }

    const clickDescription = () =>{
        setAddDescription(true)
        setEdit("Save")
    }

    const addingDescription =(e)=>{
        setDescriptionValue(e.target.value)
    }
 
    
    const clickedRemoveCheckListButton = () =>{
        setAddTitle(false)
    }

    const deleteCheckList = (e)=>{
        e.target.parentElement.parentElement.remove();
        let checkListName = e.target.parentElement.firstElementChild.textContent

        fetch(`https://api.trello.com/1/checklists/${checkListId[checkListName]}?key=${key}&token=${token}`, {
        method: 'DELETE'
        })
        .then(response => {
            console.log(
            `Response: ${response.status} ${response.statusText}`
            );
            return response.text();
        })
        .then(text => console.log(text))
        .catch(err => console.error(err));
    }

    console.log(checkListId)

    const deleteComment=(e)=>{
        e.target.parentElement.parentElement.remove()
        let commentName = e.target.parentElement.previousSibling.value
        

        fetch(`https://api.trello.com/1/cards/${cardId[cardName]}/actions/${commentIds[commentName]}/comments?key=${key}&token=${token}`, {
            method: 'DELETE'
                })
                .then(response => {
                    console.log(
                    `Response: ${response.status} ${response.statusText}`
                    );
                    return response.text();
                })

    }

    console.log(commentIds)


    if (!props.showPop) return
    return (
        <>
        <div className='mainPopUp'>
            <div className='pop-up'>
                <div className='popUpDetails'>
                    <div className='popHeading'>
                            <h3>{props.name}</h3>
                            <input className='removePopUp' type='button' onClick={props.closePop} value='X'/>
                    </div>
                    <div className='description'>
                        <h4 className='descriptionHeading'>Description</h4>
                        {/* {descriptionInput? */}
                        <p>{descriptionValue}</p>
                        {/* :null} */}
                        {!descriptionInput ?
                         <input type='text' name='textDescription' placeholder='Add a more description here' value={descriptionValue} onClick={clickDescription} onChange={addingDescription}/> 
                         : null}
                        {addDescription ? <button className='Save' onClick={clickedSave}>{edit}</button>:null}
                    </div>
                    <div className='checkListPart'>
                        
                            {
                                titleArray.map((title)=>{
                                    return <CheckList title={title} deleteCheckList={deleteCheckList} checkListId={checkListId} cardId={cardId} cardName={props.name}/>
                                })
                            }
                          
                    </div>
                    <div className='comments'>
                        <h4>Activity</h4>
                        <input className='addComments' type='text' name='addComments' placeholder='Add a comment' value={commentValue} onChange={commentInput}/>
                        <input className='saveButton' type='button' name='saveButton' value='Save' onClick={clickedCommentSaved}/>

                            <div className='commentStored'>

                        {
                            commentArray.map((comment,index)=>{
                                return < >
                                <div>
                                <input className="comments" type='text' key={index} value={comment}/>
                                <div key={index} className='editDelete'>
                                    <button className='edit'>Edit</button>
                                    <button onClick={deleteComment}>Delete</button>
                                </div>
                                </div>
                                </>
                                
                            })
                        
                        }
                            </div>
                    </div>
                </div>
                <div className='checkListHead'>
                    <p>Add to card</p>
                    <div className='checkList' onClick={clickedCheckBtn}>
                        <i class="fa-solid fa-square-check" onClick={clickedCheckBtn}></i>
                        <p className='checkiListHeading' onClick={clickedCheckBtn}>checklist</p>
                    </div>
                            {addTitle ? 
                                <div className='addingTitle'>
                                    <div className='setTitle'>
                                        <div className='addTitle'>
                                            <div className='titleHeader'>
                                                <h5>Add checklits</h5>
                                                <button onClick={clickedRemoveCheckListButton}>X</button>
                                            </div>
                                            <div className='title'>
                                                <p>Title</p>
                                                <input className="checkTitle" type='text' name='title' placeholder='Checklist' value={titleValue} onChange={checkHandler}/>
                                            </div>
                                            <div className='copyItems'>
                                                <p>Copy items from...</p>
                                                <label></label>
                                                <select name="cards" className='cardsOption' >
                                                    <option value="none">none</option>
                                                    <option value="cards">cards</option>
                                                </select>
                                            </div>
                                            <input className='submitingTitle' type='button' value='Add' onClick= {clickedAddTitle} />
                                        </div>
                                    </div>
                                </div>:null
                            }
                </div>
            </div>
        </div>
        </>
    );
};

export default Popup;