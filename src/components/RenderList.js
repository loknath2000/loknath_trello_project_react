
import React, {useState, useEffect} from 'react';
// import RenderCard from './RenderCard';
import List from './List';

const key="cbdb1f25866b842989356e653abdc925";
const token="fc4aff7f90dc80d9c9d2d69d999967daaaaf17aa737f5e67cdbb81b27af72abe";
const boardId = "624a743242f14533470abeb5";

let listId = {}

// functional component for getting the data from board
const RenderList = () => {

    const [listArray,setListArray] = useState([])

   

    useEffect (()=>{

        async function listData() {
            let response = await fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=${key}&token=${token}`, {
                method: 'GET'
            })
            return response.json();
        }

        listData().then(text=>{
            let array = []
            text.forEach(obj => {
                array.push(obj.name)

                listId[obj.name] = obj.id
                console.log(obj)
               
            })
            
            setListArray(array)
        })
        
    },[])
    

    return (
        <>
        {listArray.map(data=>{
            return  <>
                <List datas={data} listId = {listId}/>
            </>
        
        })}

       
        </>
    )
};


export default RenderList;